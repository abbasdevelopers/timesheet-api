﻿using System;
using System.Linq;
using timesheet.data;
using timesheet.model;
using Microsoft.EntityFrameworkCore;

namespace timesheet.business
{
    public class EmployeeWorkService
    {
        public TimesheetDb db { get; }
        public EmployeeWorkService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IQueryable<EmployeeWork> GetEmployeeWorks(int empid)
        {
            return this.db.EmployeeWorks
                .Include(c => c.EmployeeTask)
                .Where(e => e.EmployeeId == empid);
        }
        public EmployeeWork AddEmployeeWork(EmployeeWork employeeWork)
        {
            this.db.EmployeeWorks.Add(employeeWork);
            this.db.SaveChanges();
            return employeeWork;
        }
    }
}
