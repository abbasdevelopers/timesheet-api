﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/employee/work")]
    [ApiController]
    public class EmployeeWorkController : ControllerBase
    {
        private readonly EmployeeWorkService employeeWorkService;
        private readonly EmployeeService employeeService;
        public EmployeeWorkController(EmployeeWorkService employeeWorkService,
            EmployeeService employeeService)
        {
            this.employeeWorkService = employeeWorkService;
            this.employeeService = employeeService;
        }

        [HttpGet("get/{empid}")]
        public IActionResult GetAll(int empid)
        {
            var items = this.employeeWorkService.GetEmployeeWorks(empid);
            return new ObjectResult(items);
        }
        [HttpPost("add")]
        public IActionResult Add(EmployeeWork empWork)
        {
            var items = this.employeeWorkService.AddEmployeeWork(empWork);
            return new ObjectResult(items);
        }
    }
}