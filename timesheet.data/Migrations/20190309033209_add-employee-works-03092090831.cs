﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace timesheet.data.Migrations
{
    public partial class addemployeeworks03092090831 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_EmployeeWorks_EmployeeId",
                table: "EmployeeWorks",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeWorks_TaskId",
                table: "EmployeeWorks",
                column: "TaskId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeWorks_Employees_EmployeeId",
                table: "EmployeeWorks",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeWorks_Tasks_TaskId",
                table: "EmployeeWorks",
                column: "TaskId",
                principalTable: "Tasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeWorks_Employees_EmployeeId",
                table: "EmployeeWorks");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeWorks_Tasks_TaskId",
                table: "EmployeeWorks");

            migrationBuilder.DropIndex(
                name: "IX_EmployeeWorks_EmployeeId",
                table: "EmployeeWorks");

            migrationBuilder.DropIndex(
                name: "IX_EmployeeWorks_TaskId",
                table: "EmployeeWorks");
        }
    }
}
